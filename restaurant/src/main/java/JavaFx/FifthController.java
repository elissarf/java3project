package JavaFx;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javaproject.*;

public class FifthController {

    //elements for user input for register and login
    @FXML
    private  TextField lusername = new TextField();
    @FXML
    private  PasswordField lpassword = new PasswordField();
    @FXML
    private  TextField rusername = new TextField();
    @FXML
    private  PasswordField rpassword = new PasswordField();
    @FXML
    private  TextField fname = new TextField();
    @FXML
    private  TextField lname = new TextField();
    @FXML
    private  TextField address = new TextField();
    @FXML
    private  TextField coupon = new TextField();

    //if login, make sure not empty inputs and login works from backendservices
    @FXML
    private void login(ActionEvent event) throws IOException {
        Customer customer= 
        lusername.getText()==null || lusername.getText().isEmpty() ||
        lpassword.getText()==null || lpassword.getText().isEmpty()?
        null :
        Gui.getBs().login(lusername.getText(), lpassword.getText());

        if(customer==null){
            Gui.setRoot("failcustomer");
        }else{
            Gui.setCustomer(customer);
            Gui.setRoot("discount");
        }
    }

    //if register, make sure not empty inputs and register works from backendservices
    @FXML
    private void register(ActionEvent event) throws IOException {

        //make sure not null the variables, will still return null if username exists
        Customer customer= 
        rusername.getText()==null || rusername.getText().isEmpty() ||
        fname.getText()==null || fname.getText().isEmpty() ||
        lname.getText()==null || lname.getText().isEmpty() ||
        rpassword.getText()==null || rpassword.getText().isEmpty() ||
        address.getText()==null || address.getText().isEmpty()?
        null : 
        Gui.getBs().register(rusername.getText(), 
            fname.getText()+" "+lname.getText(), 
            rpassword.getText(), 
            address.getText());
        
        if(customer==null){
            Gui.setRoot("failcustomer");
        }else{
            Gui.setCustomer(customer);
            Gui.setRoot("discount");
        }
    }
    //for discount page, if coupon string exists in backendservices
    @FXML
    private void validateCoupon(ActionEvent event) throws IOException {
         if(!coupon.getText().isEmpty()){
            if(Gui.getBs().addCoupon(coupon.getText(), Gui.getCustomer())){
                Gui.setRoot("checkout");
            }else{
                Gui.setRoot("faildiscount");
            }
         }else{
            Gui.setRoot("checkout");
         }
         
    }
    @FXML
    private void skipDiscount(ActionEvent event) throws IOException {
       Gui.setRoot("checkout");
    }
    @FXML
    private void goBackDiscount(ActionEvent event) throws IOException {
       Gui.setRoot("discount");
    }
}
