package JavaFx;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javaproject.*;
import javaproject.restaurantfood.RestaurantFood;

public class ForthController implements Initializable {
    //just like shoppingcart, this time viewlist listens on mouseevent and removes item clicked from cart in gui
    private ObservableList<ShopCartItem> observableList = FXCollections.observableArrayList();
 
    @FXML
    private ListView<ShopCartItem> listViewCart = new ListView<>(observableList);

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        observableList.setAll(Gui.getCart().getShopCartItems());
        listViewCart.setItems(observableList);
        
    }
    @FXML
    private void openShoppingCart(ActionEvent event) throws IOException {
        Gui.setRoot("shoppingcart");
    }

    //method to remove from cart and viewlist using event clicking 
    @FXML
    private void removeItem(MouseEvent event) {
        ShopCartItem foodChosen= (ShopCartItem)listViewCart.getSelectionModel().getSelectedItem();

        if(foodChosen.getQuantity()!=1){
            ShopCartItem item= findFoodInCart(foodChosen);
            if(item!=null){
                item.setQuantity(item.getQuantity()-1);
            }
        }else{
            Gui.getCart().removeItem(foodChosen.getFood());
        }
        //remakes viewlist for new changes
        observableList.setAll(Gui.getCart().getShopCartItems());
        listViewCart.setItems(observableList);
    }
    //helper method for removeitem where it returns the cartitem that has a quantity bigger than 1 to just decrease it
    private ShopCartItem findFoodInCart(ShopCartItem foodChosen) {
        for(int i=0; i<Gui.getCart().getShopCartItems().size(); i++){
            if(Gui.getCart().getShopCartItems().get(i).getFood().displayName().equals(foodChosen.getFood().displayName())){
                return Gui.getCart().getShopCartItems().get(i);
            } 
        }
        return null;
    }
}
