package JavaFx;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javaproject.restaurantfood.RestaurantFood;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javaproject.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;


public class Gui extends Application {


    //we didn't do test on javafx controllers classes we never saw these types of tests that need to use javafx
    //used controllers to set up a connection between this gui and fxml pages 
    //create scene as well as customer and cart and order and foodfiltered (which is dynamically changing as user changes to another food) to be used
    private static Scene scene;
    //the menu is created at first 
    private static javaproject.Menu menu= new javaproject.Menu();;
    private static List<RestaurantFood> foodFiltered;
    private static ShoppingCart cart= new ShoppingCart();

    private static String firstType;

    private static Customer customer;
    private static Order order;

    //for customer handling
    static BackendService bs= new BackendServiceCSV();

    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("mainpage"), 640, 480, Color.AQUAMARINE);
        stage.setScene(scene);
        stage.show();
    }

    //the next lines are getters and setters to help connect gui with controllers
    //set foodfiltered when event is actionned
    static void createFoodList(String foodType){
        foodFiltered= menu.filter(foodType);
    }
    public static List<RestaurantFood> getFoodFiltered() {
        return foodFiltered;
    }

    public static ShoppingCart getCart(){
        return cart;
    }
    public static BackendService getBs() {
        return bs;
    }

    public static void setCustomer(Customer c) {
        customer=c;
    }
    public static Customer getCustomer() {
        return customer;
    }

    public static void setFirstType(String firstType) {
        Gui.firstType = firstType;
    }
    public static String getFirstType(){
        return firstType;
    }

    public static void createOrder() {
        order= new Order(cart, customer);
    }
    public static Order getOrder() {
        return order;
    }


    //sets root of scene to the fxml of choice
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    //uses loader to load fxml
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Gui.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    //for keeping track of foodfiltered
    public static String foodType = "";

    public static void main(String[] args) {
        launch(args);
    }
}