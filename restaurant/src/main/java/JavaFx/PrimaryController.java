package JavaFx;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.util.Callback;
import javaproject.restaurantfood.RestaurantFood;

public class PrimaryController {
    //this controller toggles between pages when buttons are clicked
    //the only use of it is setting the root to another page
    //used by many fxml

    @FXML
    private void openChoices() throws IOException {
        Gui.setRoot("menuchoices");
    }

    @FXML
    private void openSide() throws IOException {
        Gui.setFirstType("sidemenu");
        Gui.setRoot("sidemenu");
    }
    @FXML
    private void openMainFood() throws IOException {
        Gui.setFirstType("mainmenu");
        Gui.setRoot("mainmenu");
    }
    @FXML
    private void openDrink() throws IOException {
        Gui.setFirstType("drinkmenu");
        Gui.setRoot("drinkmenu");
    }
    @FXML
    private void goBack(ActionEvent event) throws IOException {
        Gui.setRoot("mainpage");
    }
    @FXML
    private void openFood(ActionEvent event) throws IOException {
        Button clicked= (Button)event.getTarget();
        Gui.foodType = clicked.getId();
        Gui.setRoot("foodlist");
    }
    
    @FXML
    private void openRegister() throws IOException {
        Gui.setRoot("register");
    }

    @FXML
    private void openLogin() throws IOException {
        Gui.setRoot("login");
    }

    @FXML
    private void goBackCustomer() throws IOException {
        Gui.setRoot("customeroptions");
    }
    @FXML
    private void showShoppingCart() throws IOException {
        Gui.setRoot("shoppingcart");
    }

    
}