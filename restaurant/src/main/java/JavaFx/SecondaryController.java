package JavaFx;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javaproject.ShopCartItem;
import javaproject.restaurantfood.RestaurantFood;

public class SecondaryController implements Initializable {
    //created listview to fill in fxml listview with same id as the variable name
    private ObservableList<RestaurantFood> observableList = FXCollections.observableArrayList();
 
    @FXML
    private ListView<RestaurantFood> listViewFood = new ListView<>(observableList);

    //runs this as soon as the fxml is created because its inzitializable
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        Gui.createFoodList(Gui.foodType);
        observableList.setAll(Gui.getFoodFiltered());
        listViewFood.setItems(observableList);
        
    }
    //the variable firsttype in gui that kept track of food filtered to return to the right page after showing list
    @FXML
    private void goBack(ActionEvent event) throws IOException {
        switch(Gui.getFirstType()){
            case "sidemenu":
                Gui.setRoot("sidemenu");
                break;
            case "mainmenu":
                Gui.setRoot("mainmenu");
                break;
            case "drinkmenu":
                Gui.setRoot("drinkmenu");
                break;
            default:
                Gui.setRoot("menuchoices");
        }
    }

    //adds to cart in gui the option in viewlist that is clicked
    @FXML
    private void chooseOption(MouseEvent event) {
        RestaurantFood foodChosen= (RestaurantFood)listViewFood.getSelectionModel().getSelectedItem();

        ShopCartItem exists= findExistingFood(foodChosen);
        if(exists!=null){
            exists.setQuantity(exists.getQuantity()+1);
        }else{
            Gui.getCart().addItem(foodChosen, 1);
        }
        System.out.println("updated cart: " + Gui.getCart());
    }
    //helper method for adding item, finds if item already added then just increases quanitity instead
    private ShopCartItem findExistingFood(RestaurantFood foodChosen) {
        for(int i=0; i<Gui.getCart().getShopCartItems().size(); i++){
            if(Gui.getCart().getShopCartItems().get(i).getFood().displayName().equals(foodChosen.displayName())){
                return Gui.getCart().getShopCartItems().get(i);
            }
        }
        return null;
    }
    //goes to shoppingcart fxml
    @FXML
    private void showShoppingCart() throws IOException {
        Gui.setRoot("shoppingcart");
    }
}
