package JavaFx;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javaproject.*;

public class SixthController implements Initializable{
    //for final page

    private ObservableList<ShopCartItem> observableList = FXCollections.observableArrayList();
 
    @FXML
    private ListView<ShopCartItem> listViewCart = new ListView<>(observableList);


    @FXML
    private TextField customerinfo = new TextField();


    @FXML
    private TextField price1 = new TextField();
    @FXML
    private TextField price2 = new TextField();
    @FXML
    private TextField price3 = new TextField();



    private static final DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
      //set customer info in textfield
        Gui.createOrder();
       customerinfo.setText(Gui.getOrder().getCustomer());

       //set cart info in viewlist
       observableList.setAll(Gui.getOrder().getCart().getShopCartItems());
        listViewCart.setItems(observableList);
        
        //set price
        double totalprice= Gui.getOrder().getTotalPrice();
        price1.setText("total price of food is : " +  df.format(totalprice) + " $");
        double discountprice= Gui.getOrder().getPriceDiscounted();
        price2.setText("total price with discount is : " + df.format(discountprice) + " $");
        double finalprice= Gui.getOrder().getPriceTaxed();
        price3.setText("final price with taxes is : " + df.format(finalprice) + " $");
    }
    
}
