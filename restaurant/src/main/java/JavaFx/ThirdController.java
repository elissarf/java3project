package JavaFx;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javaproject.*;
import javaproject.restaurantfood.RestaurantFood;

public class ThirdController implements Initializable {
    //listview to fill like secondarycontroller, this time for cart not foodfilerters
    private ObservableList<ShopCartItem> observableList = FXCollections.observableArrayList();
 
    @FXML
    private ListView<ShopCartItem> listViewCart = new ListView<>(observableList);

    //initializable like secondarycontroller
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        observableList.setAll(Gui.getCart().getShopCartItems());
        listViewCart.setItems(observableList);
        
    }
    @FXML
    private void goBack(ActionEvent event) throws IOException {
        Gui.setRoot("menuchoices");
    }
    //go to modifycart fxml
    @FXML
    private void openModifyCart(ActionEvent event) throws IOException {
        Gui.setRoot("modifycart");
    }
    //go to customeroptions fxml
    @FXML
    private void openCustomerOption() throws IOException {
        Gui.setRoot("customeroptions");
    }

}
