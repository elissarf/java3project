package javaproject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javaproject.restaurantfood.Beef;
import javaproject.restaurantfood.Chicken;
import javaproject.restaurantfood.MainMeal;
import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Rice;
import javaproject.restaurantfood.Tea;
import javaproject.restaurantfood.Veggies;

/**
 * WARNING THIS IS USELESS DO NOT READ THIS
 * DO NOT RUN THIS
 * IF YOU ARE THE TEACHER IGNORE EVERYTHING
 * we just wanted to keep this for you to see that we did an app at first for us to work with connections
 * no need to look at it
 *
 */
public class App {
    public static void main(String[] args) throws IOException {

        Customer customer= new Customer();
        // app start loop
        boolean running = true;
        while (running) {
            System.out.println("Hello, please select one of the following options:");
            System.out.println("1. Login");
            System.out.println("2. Register");
            System.out.println("3. Show Menu");
            System.out.println("4. Exit");
            String input = System.console().readLine("Input: ");
            switch (input) {
                case "1":
                    // send user to a new page to enter their credentials
                    loginPage();
                    break;
                case "2":
                    customer= registerPage();
                    break;
                case "3":
                    showMenu(customer);
                    break;
                case "5":
                    System.out.println("Admin mode enabled");
                    // List<RestaurantFood> foods = new ArrayList<>();
                    // BackendService bknd = new BackendServiceOracle();
                    // boolean works = false;
                    // foods = bknd.loadFood();
                    // for (RestaurantFood food : foods) {
                    //     if (food.displayName().equals("Chicken Soup")) {
                    //         works = true;
                    //     }
                    // }
                    // System.out.println(works); 
                    
                    break;
                    case "4":
                    System.out.println("Good bye!");
                    running = false;
                    break;
                default:
                    System.out.println("Please enter (1) or (2)");
                    break;
            }
        }
    }

    /**
     * will ask user to enter their information, display it and ask for
     * confirmation, then send them back to the main menu. This method should make a
     * call to the backend to add a new user to the database
     * 
     * @throws IOException
     */
    private static Customer registerPage() throws IOException {
        // ask the user to fill in with their information
        // username
        // loop to check if the username is already in the database
        boolean tryAgain = true;
        // while (tryAgain){
        String username = System.console().readLine("Please enter your username: ");
        // if username is not in the database
        // set tryAgain to false
        // else loop again
        // }
        // first name
        String firstName = System.console().readLine("Please enter your first name: ");
        // last name
        String lastName = System.console().readLine("Please enter your last name: ");
        // address
        String address = System.console().readLine("Please enter your address: ");
        Customer newCustomer = new Customer(username, firstName, lastName, address);
        System.out.println("Your account has been created");
        BackendServiceCSV bs = new BackendServiceCSV();
        bs.addCustomer(newCustomer);
        System.out.println(newCustomer);
        // add the newCustomer to the database
        System.out.println("New account created, you will now be sent back to the main menu...");
        return newCustomer;

    }
    private static Customer loginPage() {
        boolean running = true;
        Customer existingCustomer = null;
        while (running) {
            System.out.println("choose the next step: ");
            System.out.println("1.Provide username");
            System.out.println("2. Exit");
            String input = System.console().readLine("Input: ");
            switch (input) {
                case "1":
                    String userName = System.console().readLine("Please enter your username: ");
                    BackendService bs = new BackendServiceCSV();
                    List<Customer> customers = bs.getCustomers();
                    for (Customer customer : customers) {
                        if (bs.checkUsername(userName)) {
                            //System.out.println("Welcome " + customer.getFirstName());
                            existingCustomer = customer;
                            running = false;
                        }
                    }
                if (existingCustomer==null) {
                System.out.println("Wrong username, please try again.");}
                break;
                case "2":
                    running = false;
                    break;
                default:
                System.out.println("Wrong input, please enter 1 or 2.");
            }
        }
        return existingCustomer;
    }

    // testing the menu class
    public static void showMenu(Customer customer) {
        Menu menu= new Menu();
        MenuActivity activity= new MenuActivity(menu, customer);
        // show the three options there is
        boolean running = true;
        while (running) {
            System.out.println("here is the menu, choose: ");
            System.out.println("1. Main meal");
            System.out.println("2. Sides");
            System.out.println("3. Drinks");
            System.out.println("4. Exit");
            String input = System.console().readLine("Input: ");
            
            switch (input) {
                case "1":
                    showMainMeals(activity);
                    System.out.println(activity);
                    break;
                case "2":
                    showSides(activity);
                    System.out.println(activity);;
                    break;
                case "3":
                    showDrinks(activity);
                    System.out.println(activity);;
                    break;
                case "4":
                    System.out.println("Good bye!");
                    running = false;
                    break;
                default:
                    System.out.println("Please enter (1) or (2)");
                    break;
            }
        }


    }

    private static void showDrinks(MenuActivity activity) {
        // show the options there is
        boolean running = true;
        while (running) {
            System.out.println("here is the drink options, choose: ");
            System.out.println("1. Fizzy");
            System.out.println("2. Juice");
            System.out.println("3. Tea");
            System.out.println("4. Exit");
            String input = System.console().readLine("Input: ");
            
            switch (input) {
                case "1":
                    activity.filter("Fizzy");
                    running = false;
                    break;
                case "2":
                    activity.filter("Juice");
                    running = false;
                    break;
                case "3":
                    activity.filter("Tea");
                    running = false;
                    break;
                case "4":
                    System.out.println("Good bye!");
                    running = false;
                    break;
                default:
                    System.out.println("Please enter (1) or (2)");
                    break;
            }
        }
    }

    private static void showSides(MenuActivity activity) {
        // show the options there is
        boolean running = true;
        while (running) {
            System.out.println("here is the side options, choose: ");
            System.out.println("1. Fries");
            System.out.println("2. Rice");
            System.out.println("3. Veggies");
            System.out.println("4. Exit");
            String input = System.console().readLine("Input: ");
            
            switch (input) {
                case "1":
                    activity.filter("Fries");
                    running = false;
                    break;
                case "2":
                    activity.filter("Rice");
                    running = false;
                    break;
                case "3":
                    activity.filter("Veggies");
                    running = false;
                    break;
                case "4":
                    System.out.println("Good bye!");
                    running = false;
                    break;
                default:
                    System.out.println("Please enter (1) or (2)");
                    break;
            }
        }
    }

    private static void showMainMeals(MenuActivity activity) {
        // show the options there is
        boolean running = true;
        while (running) {
            System.out.println("here is the meal options, choose: ");
            System.out.println("1. Beef");
            System.out.println("2. Chicken");
            System.out.println("3. Fish");
            System.out.println("4. Pasta");
            System.out.println("5. Exit");
            String input = System.console().readLine("Input: ");
            
            switch (input) {
                case "1":
                    activity.filter("Beef");
                    running = false;
                    break;
                case "2":
                    activity.filter("Chicken");
                    running = false;
                    break;
                case "3":
                    activity.filter("Fish");
                    running = false;
                    break;
                case "4":
                    activity.filter("Pasta");
                    running = false;
                    break;
                case "5":
                    System.out.println("Good bye!");
                    running = false;
                    break;
                default:
                    System.out.println("Please enter (1) or (2)");
                    break;
            }
        }
    }
}
