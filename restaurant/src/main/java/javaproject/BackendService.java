package javaproject;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javaproject.discount.CouponDiscount;
import javaproject.discount.Discount;
import javaproject.restaurantfood.RestaurantFood;

public interface BackendService {
    public List<Customer> getCustomers();
    /**
     * If credentials do not exist in the customers table return a null value. (wrap login in a while false loop until the user enters valid credentials)
     *
     * If the  firstName and lastName of the customer is present in the employees database the customer object will also have an employee Discount in the discount field. 
     * Otherwise after a successful login the customer wil be prompted to enter a coupon code that will be then passed to checkCoupon method.
     * @param userId
     * @param lastName
     * @return customer object
     */
    public Customer login(String userId, String lastName);

    /**
     * Will return null if the userName is already taken
     * (wrap register in a while false loop until the user enters a valid userName)
     * the customer object will also have newCustomer discount in their discount field. But will not exist when they attempt to login again.
     * @param userName
     * @param firstName
     * @param lastName
     * @param address
     * @return customer object
     */
    public Customer register(String userName, String firstName, String lastName, String address);

    public List<RestaurantFood> loadFood() throws Exception;

    public void addCustomer(Customer customer);

    public List<String[]> getEmployees();

    /**
     * Will query the customers table with the given userName to see if its already in use.
     * @param userName
     * @return boolean
     */
    public boolean checkUsername(String userName);

    /**
     * Query the employee database to see if the given first name and last name are already present.
     * @param firstName
     * @param lastName
     * @return boolean
     */
    public boolean isAnEmployee(String name);

    /**
     * return an array list of all the coupons present in the coupons table
     * @param couponCode
     * @return
     */
    public List<CouponDiscount> getCoupons();

    /**
     * once a user logs in or registers we will allow them to apply a coupon if they do not already have a dicount in their customer object. 
     * @param couponCode
     * @param user
     * @return
     */
    public boolean addCoupon(String couponCode, Customer user);
}
