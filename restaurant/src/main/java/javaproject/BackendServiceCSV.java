package javaproject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
//import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javaproject.discount.CouponDiscount;
import javaproject.discount.Discount;
import javaproject.discount.EmployeeDiscount;
import javaproject.discount.NewCustomerDiscount;
import javaproject.restaurantfood.RestaurantFood;

//this class will handle all interactions between our frontend and backed
//each of our objects should have their own add to database methods
public class BackendServiceCSV implements BackendService {
    // interactions with a .csv file system database
    public BackendServiceCSV() {
    }

    public String getType() {
        return "BackendServices object";
    }

    @Override
    public List<String[]> getEmployees() {
        try {
            // try block to interact with the customers table file
            Path file = Paths.get("src/main/java/javaproject/backend/employees.csv");
            List<String> rows;

            rows = Files.readAllLines(file);
            ArrayList<String[]> employeeNames = new ArrayList<String[]>();
            for (String row : rows) {
                String[] columns = row.split("#");
                // make a 2 length array to store each firstName and lastName per employee entry
                String[] employee = { columns[0], columns[1] };
                employeeNames.add(employee);
            }
            return employeeNames;
        } catch (IOException e) {
            // else send an alert to the user and return null
            System.out.println("Unable to get all employees: unable locate employees.csv");
            return null;
        }
    }

    @Override
    public List<Customer> getCustomers() {
        try {
            // try block to interact with the customers table file
            Path file = Paths.get("src/main/java/javaproject/backend/customers.csv");
            List<String> rows;

            rows = Files.readAllLines(file);
            ArrayList<Customer> customers = new ArrayList<Customer>();
            for (String row : rows) {
                String[] columns = row.split("#");
                customers.add(new Customer(columns[0], columns[1], columns[2], columns[3]));
            }
            return customers;
        } catch (IOException e) {
            // else send an alert to the user and return null
            System.out.println("Unable to get all customers: unable locate customer.csv");
            return null;
        }
    }

    @Override
    public void addCustomer(Customer customer) {
        try {
            File file = new File("src/main/java/javaproject/backend/customers.csv");
            FileWriter fr = new FileWriter(file, true);
            fr.write(customer.getAsRow());
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Unable to add customer: can't find customers.csv file");
        }
    }

    @Override
    public boolean checkUsername(String userName) {
        List<Customer> customers = getCustomers();
        Boolean match = false;
        for (Customer customer : customers) {
            if (customer.getUserName().equals(userName)) {
                match = true;
            }
        }
        return match;
    }

    @Override
    public List<RestaurantFood> loadFood() throws IOException {
        List<RestaurantFood> food = new ArrayList<RestaurantFood>();
        // not good practice to name folder backend for csv: instead resources
        Path p = Paths.get(
                "src/main/java/javaproject/backend/restarurantFood.csv");
        List<String> linesOfFile = Files.readAllLines(p);

        RestaurantFoodFactory factory = new RestaurantFoodFactory();

        for (String line : linesOfFile) {
            String[] columns = line.split(",");
            // STring, String
            Map<String, String> probs = new HashMap<>();
            probs.put("name", columns[1]);
            probs.put("price", columns[2]);
            probs.put("calories", columns[3]);
            probs.put("ingredients", columns[4]);

            food.add(factory.createRestaurantFood(columns[0], probs));

        }
        return food;
    }
    // update the csv row column of points of customer object that exists (replace
    // object prob with updated version)

    @Override
    public boolean isAnEmployee(String name) {
        Boolean bool = false;
        List<String[]> employees = getEmployees();
        for (String[] employee : employees) {
            String fullname= employee[0] + " " + employee[1];
            if (fullname.equals(name)) {
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public boolean addCoupon(String couponCode, Customer user) {
       //check if the couponCode is valid
       boolean success = false;
       List<CouponDiscount> coupons = getCoupons();
       for(CouponDiscount i : coupons ){
        if(i.getCode().equals(couponCode)){
            user.addDiscount((Discount)i);
            success =  true;
        }
       }
       return success;
    }
    @Override
    public List<CouponDiscount> getCoupons() {
        try {
            // try block to interact with the customers table file
            Path file = Paths.get("src/main/java/javaproject/backend/coupons.csv");
            List<String> rows;

            rows = Files.readAllLines(file);
            ArrayList<CouponDiscount> coupons = new ArrayList<CouponDiscount>();
            for (String row : rows) {
                String[] columns = row.split("#");
                // make a 2 length array to store each firstName and lastName per employee entry
                CouponDiscount c = new CouponDiscount(columns[0], Integer.parseInt(columns[1]));
            coupons.add(c);
            }
            return coupons;
        } catch (IOException e) {
            // else send an alert to the user and return null
            System.out.println("Unable to get all coupons: unable locate coupons.csv");
            return null;
        }
    }

    @Override
    public Customer login(String username, String password) {
        Customer match = null;
        // look for the userId after fetching all the customers
        List<Customer> customers = getCustomers();
        for (Customer i : customers) {
            if (i.getUserName().equals(username) && i.getPassword().equals(password)) {
                match = i;
                if (isAnEmployee(i.getName())) {
                    match.addDiscount(new EmployeeDiscount());
                }
            }
        }
        return match;
    }

    @Override
    public Customer register(String userName, String name, String password, String address) {
        if (!checkUsername(userName)) {
            // check if the new user is also an employee
            Customer regCustomer = new Customer(userName, name, password, address);
            if (isAnEmployee(name)) {
                regCustomer.addDiscount(new EmployeeDiscount());
                addCustomer(regCustomer);
                return regCustomer;
            } else {
                regCustomer.addDiscount(new NewCustomerDiscount());
                addCustomer(regCustomer);
                return regCustomer;
            }
        }
        return null;
    }

}
