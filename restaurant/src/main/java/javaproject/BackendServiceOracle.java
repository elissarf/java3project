package javaproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javaproject.discount.CouponDiscount;
import javaproject.restaurantfood.RestaurantFood;

public class BackendServiceOracle implements BackendService {
    
    private String username;
    private String password;
    private Connection conn;

    // interactions with an oracle database
    public BackendServiceOracle() {
        String user = System.console().readLine("User name : ");
          char[] charPwd = System.console().readPassword("Password : ");
          String pwd = new String(charPwd);
        this.username = user;
        this.password = pwd;
        try {
            this.conn = getConn();
            System.out.println(this.conn);
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    /**
     * @return Connection type object
     */
    private Connection getConn() throws SQLException {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn = DriverManager.getConnection(url, this.username, this.password);
        return conn;
    }

    @Override
    public List<Customer> getCustomers() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addCustomer(Customer customer) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean checkUsername(String userName) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<RestaurantFood> loadFood() throws IOException {
        List<RestaurantFood> food = new ArrayList<RestaurantFood>();
        RestaurantFoodFactory factory = new RestaurantFoodFactory();
        Map<String, String> probs = new HashMap<>();
        String sql = "SELECT * FROM FOOD";
        try {
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                 probs.put("name",result.getString("food_name"));
                probs.put("price",result.getDouble("price")+"");
                probs.put("calories",result.getInt("calories")+"");
                probs.put("ingredients",result.getString("ingredients"));
                food.add(factory.createRestaurantFood(result.getString("food_type"), probs));
            }
        }
        catch (SQLException e) {
            throw new IOException(e);
        }
        return food;
    }


    @Override
    public List<String[]> getEmployees() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isAnEmployee(String name) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public Customer login(String userId, String lastName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Customer register(String userName, String firstName, String lastName, String address) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<CouponDiscount> getCoupons() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean addCoupon(String couponCode, Customer user) {
        // TODO Auto-generated method stub
        return false;
    }

}
