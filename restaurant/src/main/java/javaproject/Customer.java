package javaproject;

import java.util.ArrayList;
import java.util.List;

import javaproject.discount.CouponDiscount;
import javaproject.discount.Discount;

public class Customer {

    private String userName;
    private String name;
    private String password;
    private String address;
    private List<Discount> discounts= new ArrayList<>();

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void addDiscount(Discount discount) {
        this.discounts.add(discount);
    }

    // flow for resgisterng
    // register sends user to app, then at the end of the order the customer is
    // added to databse
    public Customer(String userName, String name, String password, String address) {
        // upon cration of the customer object we check if they qualify for employee
        // discount
        this.userName = userName;
        this.name = name;
        this.password = password;
        this.address = address;
    }

    public Customer() {
    }

    public String getUserName() {
        return userName;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public String getAsRow() {
        // add each field of the object into one string matching the structuire of the
        // csv file
        String row = "\n" + this.userName + "#";
        row += this.name + "#";
        row += this.password + "#";
        row += this.address;
        return row;
    }

    @Override
    public String toString() {
        String builder= "Username:"+ userName + "------"+ 
            "firstName: " + name + "-------"+
            "password: " + password
                + "-------"+ "address: "
                + address + "--------" + "Discount:";

        for(Discount d : discounts){
            builder+=" "+ d + "-------";
        }
        return builder;
    }
}
