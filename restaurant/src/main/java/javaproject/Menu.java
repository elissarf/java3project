package javaproject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javaproject.restaurantfood.RestaurantFood;

public class Menu {
    private List<RestaurantFood> allFoods;

    //menu displays allfood
    public Menu() {
        //loading the food into a list
        BackendService bs = new BackendServiceCSV();
        try{
            this.allFoods = bs.loadFood();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
     public List<RestaurantFood> getAllFoods() {
         return allFoods;
     }
    //useless just to test
    public String toString() {
        String builder ="";
        for (int i=0 ; i<this.allFoods.size() ; i++ ) {
            builder += i + ": " + this.allFoods.get(i) + "\n";
        }
        return builder;
    }

    public List<RestaurantFood> filter(final String typeChosen) {
        List<RestaurantFood> items= new ArrayList<>();
        
        for(int i=0; i<allFoods.size(); i++){
            if(allFoods.get(i).getClass().getSimpleName().equals(typeChosen)) {
                items.add(allFoods.get(i));
            }
        }
        return items;
    }
}
