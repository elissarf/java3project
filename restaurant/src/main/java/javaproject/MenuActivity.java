package javaproject;

import java.util.ArrayList;
import java.util.List;

import javaproject.restaurantfood.RestaurantFood;

public class MenuActivity {
    private List<RestaurantFood> selectedFoodTypeItems;
    private Menu menu;
    private ShoppingCart cart;

    //intermediate betweeen menu and shopping cart
    //make the filter methods into shopping cart class
    //menu displays allfood
    //menuFilterers displays specificfoodfiltered
    public MenuActivity(Menu menu, Customer customer) {
        this.menu=menu;
        //identifying an empty selectedFoodTypeItems list
        this.selectedFoodTypeItems = new ArrayList<RestaurantFood>();
        this.cart= new ShoppingCart();
        
    }
     public void filter(String typeChosen) {
        selectedFoodTypeItems.clear();
        selectedFoodTypeItems.addAll(menu.filter(typeChosen));
    }
    //displays the selected food type items
    //useless just to test
    public String toString() {
        String builder ="";
        for(int i=0 ; i<this.selectedFoodTypeItems.size() ; i++ ){
            builder += this.selectedFoodTypeItems.get(i) + "\n";
        }
        return builder;
    }

    public ShoppingCart getCart() {
        return cart;
    }
}
