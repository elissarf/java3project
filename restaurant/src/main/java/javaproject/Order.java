package javaproject;

import javaproject.discount.*;

public class Order {
    ShoppingCart cart;
    Customer customer;
    //intermediate between cart and customer to get discount from customer into cart
    //this is when shopping cart is finizlied, like checkingout

    public Order(ShoppingCart cart, Customer customer){
        this.cart=cart;
        this.customer=customer;
    }

    //get total price of shopping cart
    //get different types of prices in the next methods, read headers
    public double getTotalPrice(){
        double price=0;
        for(int i=0; i<cart.getShopCartItems().size(); i++){
            int quantity= cart.getShopCartItems().get(i).getQuantity();
           price+=cart.getShopCartItems().get(i).getFood().calculatePrice()*quantity;
       }
        return price;
    }
    public double getPriceDiscounted(){
        double total= getTotalPrice();
        return total - (total*getTotalDiscount()/100);
    }

    public double getPriceTaxed(){
        return getPriceDiscounted() * 1.15;
    }

    public int getTotalDiscount(){
        int total=0;
        for(int i=0; i<customer.getDiscounts().size(); i++){
            total+=customer.getDiscounts().get(i).getDiscount();
        }
        return total;
    }
    public String getCustomer() {
        return customer.toString();
    }

    public ShoppingCart getCart() {
        return cart;
    }
}
