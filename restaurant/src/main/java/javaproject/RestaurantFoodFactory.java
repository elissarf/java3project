package javaproject;

import java.util.Map;

import javaproject.restaurantfood.*;

public class RestaurantFoodFactory {
    //this is a factory that takes a map and creates a class based on the class name from clazz string
    //used from backendservices to load food
    public RestaurantFood createRestaurantFood(String clazz, Map<String, String> probs){
        if(clazz==null || clazz.isEmpty()){
            return null;
        }
        switch (clazz){
            case "Beef":
                return new Beef(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Chicken":
                return new Chicken(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Fish":
                return new Fish(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Pasta":
                return new Pasta(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Fizzy":
                return new Fizzy(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Juice":
                return new Juice(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Tea":
                return new Tea(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Fries":
                return new Fries(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Rice":
                return new Rice(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            case "Veggies":
                return new Veggies(probs.get("name"), Double.parseDouble(probs.get("price")), Integer.parseInt(probs.get("calories")), probs.get("ingredients"));
            default:
                return null;
        }
}
}


