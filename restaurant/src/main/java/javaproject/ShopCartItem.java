package javaproject;

import javaproject.restaurantfood.RestaurantFood;

public class ShopCartItem {
    //one item in a shopping cart that contains food and its quantity
    //this way we dont repeat same food in a shopping cart, we just change its quantity
    private RestaurantFood food;
    private int quantity;

    public ShopCartItem(RestaurantFood food, int quantity){
        this.food=food;
        this.quantity=quantity;
    }

    public RestaurantFood getFood() {
        return food;
    }
    public void setFood(RestaurantFood food) {
        this.food = food;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public int getQuantity() {
        return quantity;
    }
    public String toString(){
        return food + " quantity: " + quantity;
    }
}
