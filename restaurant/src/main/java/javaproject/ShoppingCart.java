package javaproject;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javaproject.restaurantfood.RestaurantFood;

public class ShoppingCart {


    //only contains food types picked from customer

    private List<ShopCartItem> shopCartItems= new ArrayList<>();

    public ShoppingCart(){
    }

    public List<ShopCartItem> getShopCartItems() {
        return shopCartItems;
    }

    public void addItem(RestaurantFood food, int quantity){
        ShopCartItem item= new ShopCartItem(food, quantity);
        shopCartItems.add(item);
    }

    public void removeItem(final RestaurantFood food){
        Predicate<ShopCartItem> condition= createCondition(food);
        //this removes if the condition is true
        //the condition is that if the object when u return its RestaurantFood type, 
        //it returns true on its overriden equals method that the food i chose is the same as the food from the item list
        shopCartItems.removeIf(condition);
    }

    //never used --recently commented
    public void removeAll(){
        //when order is created successfully 
        shopCartItems.clear();
    }

    //method to return the condition where the food is the same as an item from the list
    private Predicate<ShopCartItem> createCondition(final RestaurantFood food){
        Predicate<ShopCartItem> condition = new Predicate<ShopCartItem>() 
        {
            @Override
            public boolean test(final ShopCartItem obj) {
                if (obj.getFood().equals(food)) {
                    return true;
                }
                return false;
            }
        };
        return condition;
    }

    public String toString(){
        String builder ="";
        for(int i=0 ; i<this.shopCartItems.size() ; i++ ){
            builder += i+1 + ": " + this.shopCartItems.get(i) + "\n";
        }
        return builder;
    }
}
