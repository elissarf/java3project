package javaproject.discount;

public class CouponDiscount implements Discount {
	String code;
	//this is also percentage like employee and newcustomer discount
	int amount;

	public CouponDiscount(String code, int amount) {
		this.code = code;
		this.amount = amount;
	}

	@Override
	public int getDiscount() {
		return amount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "CouponDiscount [code=" + code + ", discount=" + amount + "%]";
	}

}
