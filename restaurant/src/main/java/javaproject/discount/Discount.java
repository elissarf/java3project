package javaproject.discount;
public interface Discount {
  public int getDiscount();
}
