package javaproject.discount;

public class EmployeeDiscount implements Discount {
    private final int percent = 10;

    @Override
    public int getDiscount() {
        return percent;
    }
    @Override
	public String toString() {
		return "Employee Discount: 10%";
	}
}
