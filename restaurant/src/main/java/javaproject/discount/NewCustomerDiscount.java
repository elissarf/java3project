package javaproject.discount;

public class NewCustomerDiscount implements Discount {
    private final int percent;
    
    public NewCustomerDiscount(){
        this.percent = 10;
    }

    @Override
    public int getDiscount() {
        return percent;
    }
    @Override
	public String toString() {
		return "New customer Discount: 10%";
	}

}
