create table food (
    food_name varchar2(20) NOT NULL,
    food_type varchar2(20) NOT NULL,
    price number(4,2) NOT NULL,
    Calories Number(2) NOT NULL,
    Ingredients varchar2(700) NOT NULL
)
 
insert into food values ('Burger','Beef',7.99,450,'bun,mustard,ketchup,beef meat,salt,lettuce');
insert into food values ('Chicken Soup','Chicken',6.99,390,'chicken,chicken broth,mushroom');
insert into food values ('Lemonade','Juice',2.79,120,'lemons,sugar,water');