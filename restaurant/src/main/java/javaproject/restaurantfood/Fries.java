package javaproject.restaurantfood;

public class Fries extends Side{

    public Fries(String name, double price, int calories, String ingredients) {
        super(name, price, calories, ingredients);
    }
    
}
