package javaproject.restaurantfood;

public class Juice extends Drink {

    public Juice(String name, double price, int calories, String ingredients) {
        super(name, price, calories, ingredients);
    }
    
}
