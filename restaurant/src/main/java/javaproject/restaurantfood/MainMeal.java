package javaproject.restaurantfood;

import java.util.ArrayList;
import java.util.List;

public abstract class MainMeal implements RestaurantFood {
    private String name;
    private double price;
    private int calories;
    private String ingredients;
    // avoid null pointer exceptions
    private List<RestaurantFood> complements = new ArrayList<>();


    
    //will erase idea of sides in mainmeal

    // constructer
    public MainMeal(String name, double price, int calories, String ingredients) {
        this.name = name;
        this.price = price;
        this.calories = calories;
        this.ingredients = ingredients;
    }

    // setters
    public void setComplements(List<RestaurantFood> complements) {
        this.complements = complements;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    // getters
    public List<RestaurantFood> getComplements() {
        return complements;
    }

    public int getCalories() {
        return calories;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getIngredients() {
        return ingredients;
    }

    // extended methods
    public int calculateCalories() {
        // quiker way, doesnt work with my java version setup
        // return calories + this.complements.stream().map(x ->
        // x.calculateCalories()).reduce(0, (a, b) -> a + b);
        int totalCalories = this.calories;
        for (int i = 0; i < complements.size(); i++) {
            totalCalories += complements.get(i).calculateCalories();
        }
        return totalCalories;
    }

    public double calculatePrice() {
        double totalPrice = this.price;
        for (int i = 0; i < complements.size(); i++) {
            totalPrice += complements.get(i).calculatePrice();
        }
        return totalPrice;
    }

    public String displayName() {
        return name;
    }

    public String describe() {
        String allIngredients = this.name + ": " + this.ingredients + "\n";
        for (int i = 0; i < complements.size(); i++) {
            allIngredients += complements.get(i).displayName() + ": " + complements.get(i).describe() + "\n";
        }
        return allIngredients;
    }
    public String toString(){
        String builder=this.name.toUpperCase() + "\n" +
            "       ->price: " + this.price + "\n" +
            "       ->calories: " + this.calories + "\n" +
            "       ->ingredients: " + "\n";

        String[] breakdown= this.ingredients.split(";");
        for(String one : breakdown){
            builder += "            -" + one + "\n";
        }
        return builder;
    }

    public boolean equals(Object o){
        if(o==null || !o.getClass().equals(this.getClass())){
            return false;
        }
        else if(this.name.equals(((RestaurantFood) o).displayName())){
            return true;
        }
        else{
            return false;
        }
    }
}
