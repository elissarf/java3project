package javaproject.restaurantfood;

public interface RestaurantFood {
    int calculateCalories();
    double calculatePrice();
    String displayName();
    String describe();    
}
