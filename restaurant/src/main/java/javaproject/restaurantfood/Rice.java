package javaproject.restaurantfood;

public class Rice extends Side{

    public Rice(String name, double price, int calories, String ingredients) {
        super(name, price, calories, ingredients);
    }
    
}
