package javaproject.restaurantfood;

public abstract class Side implements RestaurantFood {
    private String name;
    private double price;
    private int calories;
    private String ingredients;

    public Side(String name, double price, int calories, String ingredients) {
        this.name = name;
        this.price = price;
        this.calories = calories;
        this.ingredients = ingredients;
    }

    // setters
    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    // getters
    public int getCalories() {
        return calories;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getIngredients() {
        return ingredients;
    }

    // extended methods
    public int calculateCalories() {
        return calories;
    }

    public double calculatePrice() {
        return price;
    }

    public String displayName() {
        return name;
    }

    public String describe() {
        return ingredients;
    }
    public String toString(){
        String builder=this.name.toUpperCase() + "\n" +
            "       ->price: " + this.price + "\n" +
            "       ->calories: " + this.calories + "\n" +
            "       ->ingredients: " + "\n";

        String[] breakdown= this.ingredients.split(";");
        for(String one : breakdown){
            builder += "            -" + one + "\n";
        }
        return builder;
    }

    public boolean equals(Object o){
        if(o==null || !o.getClass().equals(this.getClass())){
            return false;
        }
        else if(this.name.equals(((RestaurantFood) o).displayName())){
            return true;
        }
        else{
            return false;
        }
    }
}
