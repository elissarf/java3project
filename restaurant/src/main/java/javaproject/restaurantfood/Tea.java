package javaproject.restaurantfood;

public class Tea extends Drink{

    public Tea(String name, double price, int calories, String ingredients) {
        super(name, price, calories, ingredients);
    }
    
}
