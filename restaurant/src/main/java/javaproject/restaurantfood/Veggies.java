package javaproject.restaurantfood;

public class Veggies extends Side {

    public Veggies(String name, double price, int calories, String ingredients) {
        super(name, price, calories, ingredients);
    }
    
}
