module JavaFx  {
        requires javafx.controls;
        requires javafx.fxml;
        requires java.sql;
    
    
        opens JavaFx to javafx.fxml;
        exports JavaFx;
    
}
