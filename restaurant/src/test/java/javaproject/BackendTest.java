package javaproject;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import javaproject.restaurantfood.RestaurantFood;

import javaproject.discount.CouponDiscount;

public class BackendTest {
    /**
     * Test checkUsername() function by adding a customer and then checking if it is
     * present in the customers csv file. We use addCustomer() to add a fake customer
     * and also getCustomers() which is already called in the checkUsername() to retrieve all customers. 
     * So these two methods will be tested too.
     */
    @Test
    public void testGetCustomer() {
        Customer test = new Customer("tes123", "john", "doe", "404 notFound");
        BackendServiceCSV bknd = new BackendServiceCSV();
        bknd.addCustomer(test);
        boolean check = bknd.checkUsername("tes123");
        assertTrue(check);
    }
    /**
     * Test loadFood() function by storing it in an array 
     * and iterating the array to see if it has one of the food items (Chicken soup for example).
     * @throws Exception
     */
    @Test
    public void testLoadFood() throws Exception {
        List<RestaurantFood> foods = new ArrayList<>();
        BackendService bknd = new BackendServiceCSV();
        boolean works = false;
        foods = bknd.loadFood();
        for ( RestaurantFood food : foods ) {
            if (food.displayName().equals("chicken soup")){
                works = true;
            }   
        }
        assertTrue(works);
    }

    BackendServiceCSV bCsv = new BackendServiceCSV();

    @Test
    public void testGetCustomers(){
        List<Customer> customers = bCsv.getCustomers();
        for(Customer i : customers){
            System.out.println(i);
        }

    }
    @Test
    public void testAddCustomer() {
        Customer test = new Customer("test123", "john", "doe", "404 notFound");
        bCsv.addCustomer(test);
    }

    @Test
    public void testCheckUserName() {
        bCsv.checkUsername("tes123");
    }

    @Test
    public void testGetEmployees() {
        List<String[]> employees = bCsv.getEmployees();
        for (String[] employee : employees) {
            System.out.println(employee[0] + " " + employee[1]);
        }
    }

    @Test
    public void testIsAnEmployee() {
        assertTrue(bCsv.isAnEmployee("elissar " + "fadel"));
    }

    @Test
    public void testLogin() {
        Customer test = bCsv.login("123","pass");
        assertTrue( test.getPassword().equals("pass"));
    }

    /**
     * must remember to change userid and firstname before every test
     */
    //we decided to delete this by the end
    // @Test
    // public void testRegister(){
    //     assertTrue(bCsv.register("123", "elissar", "pass", "4569 crime street") != null); 

    // }

    @Test
    public void testAddCoupon(){
         Customer bob = new Customer("richard432", "Thomas", "Henry", "4367 rue Geoffrey");
         bCsv.addCoupon("red42trew", bob);
         System.out.println(bob);
    }
    @Test
    public void testGetCoupons(){
       List<CouponDiscount> coupons =  bCsv.getCoupons();
       for (CouponDiscount i : coupons){
        System.out.println(i);
       }
    }
}
