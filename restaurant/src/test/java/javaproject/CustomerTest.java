package javaproject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
public class CustomerTest {
    @Test
    public void testCustomerConstructor() {
        Customer customer = new Customer("test","alex","123","montreal");
        boolean works = false;
        if (customer.getUserName().equals("test")&&customer.getName().equals("alex")&&customer.getPassword().equals("123")&&customer.getAddress().equals("montreal")){
            works = true;
        }
        assertTrue(works);
    }
}
