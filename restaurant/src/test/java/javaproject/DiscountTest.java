package javaproject;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import javaproject.discount.CouponDiscount;
import javaproject.discount.Discount;
import javaproject.discount.EmployeeDiscount;
import javaproject.discount.NewCustomerDiscount;
public class DiscountTest {
    @Test
    public void testCouponDiscount() {
        Discount discount = new CouponDiscount("521212", 20);
        boolean works = false;
        if (discount.getDiscount()==20){
            works = true;
        }
        assertTrue(works);
    }
    @Test
    public void testEmployeeDiscount() {
        Discount discount = new EmployeeDiscount();
        boolean works = false;
        if (discount.getDiscount()==10){
            works = true;
        }
        assertTrue(works);
    }
    @Test
    public void testNewCustomerDiscount() {
        Discount discount = new NewCustomerDiscount();
        boolean works = false;
        if (discount.getDiscount()==10){
            works = true;
        }
        assertTrue(works);
    }
}
