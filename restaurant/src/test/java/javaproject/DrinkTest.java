package javaproject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Tea;

public class DrinkTest {
    public static final double DELTA = 1e-15;
    /**
     * Test calculateCalories() to check if it returns the total calories of the example drink
     */
    @Test
    public void testCalculateCalories() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        assertEquals(20, drink.calculateCalories());
    }
    /**
     * Test calculatePrice() to check if it returns the total price of the example drink
     */
    @Test
    public void testCalculatePrice() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        assertEquals(3.50, drink.calculatePrice(),DELTA);
    }

    /**
     * Test describe() to check if it returns an string which has all of the ingredients of the example drink 
     */
    @Test
    public void testDescribe() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        String expected = "water,tea";
        assertEquals( expected,drink.describe());
    }
}
