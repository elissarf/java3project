package javaproject;

import static org.junit.Assert.assertTrue;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import javaproject.restaurantfood.Beef;

public class FoodFactoryTest {
    /**
     * Test createRestaurantFood() to check if it returns the correct food type object based on 
     * the class name and the map that we pass to it 
     */
    @Test
    public void testCreateRestaurantFood() {

        RestaurantFoodFactory factory= new RestaurantFoodFactory();
        Map<String, String> probs= new HashMap<>();
        boolean works = false;
        //beef example
            probs.put("name", "burger");
            probs.put("price", "7.99");
            probs.put("calories", "430" );
            probs.put("ingredients", "something");
            if (factory.createRestaurantFood("Beef", probs) instanceof Beef){
                works = true;
            }
        assertTrue(works);
    }
}
