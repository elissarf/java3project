package javaproject;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import javaproject.restaurantfood.Beef;
import javaproject.restaurantfood.MainMeal;
import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Rice;
import javaproject.restaurantfood.Tea;

public class MainMealTest {
    public static final double DELTA = 1e-15;
    /**
     * Test calculateCalories() to check if it returns the total calories of the example food and the compliments
     */
    @Test
    public void testCalculateCalories() {
        List<RestaurantFood> complements = new ArrayList<>();
        MainMeal meal = new Beef("Pizza", 10.99, 100, "cheese,tomato,peperoni");
        RestaurantFood side = new Rice("white rice", 5.00, 50, "rice");
        RestaurantFood drink = new Tea("green tea", 3.00, 20, "water,tea");
        complements.add(side);
        complements.add(drink);
        meal.setComplements(complements);
        assertEquals(170, meal.calculateCalories());
    }
    /**
     * Test calculatePrice() to check if it returns the total price of the example food and the compliments
     */
    @Test
    public void testCalculatePrice() {
        List<RestaurantFood> complements = new ArrayList<>();
        MainMeal meal = new Beef("Pizza", 12.50, 100, "cheese,tomato,peperoni");
        RestaurantFood side = new Rice("white rice", 5.00, 50, "rice");
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        complements.add(side);
        complements.add(drink);
        meal.setComplements(complements);
        assertEquals(21.00, meal.calculatePrice(),DELTA);
    }

    /**
     * Test describe() to check if it returns an string which has all of the ingredients of the example food and the compliments
     */
    @Test
    public void testDescribe() {
        List<RestaurantFood> complements = new ArrayList<>();
        MainMeal meal = new Beef("Pizza", 12.50, 100, "cheese,tomato,peperoni");
        RestaurantFood side = new Rice("white rice", 5.00, 50, "rice");
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        complements.add(side);
        complements.add(drink);
        meal.setComplements(complements);
        String expected = "Pizza: cheese,tomato,peperoni\nwhite rice: rice\ngreen tea: water,tea\n";
        assertEquals( expected,meal.describe());
    }
}
