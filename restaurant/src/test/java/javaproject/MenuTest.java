package javaproject;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.List;
import org.junit.Test;
import javaproject.restaurantfood.RestaurantFood;

public class MenuTest {
    /**
     * Test filter() in Menu to check if it returns an array of foods only with given type name
     * @throws IOException
     */
    @Test
    public void testMenu() throws IOException {
        Menu menu = new Menu();
        boolean works = true;
        List<RestaurantFood> returnedFood = menu.filter("Beef");
        for (RestaurantFood food : returnedFood) {
            if(!food.getClass().getSimpleName().equals("Beef")){
                works = false;
            }
        }
        assertTrue(works);
    }
}
