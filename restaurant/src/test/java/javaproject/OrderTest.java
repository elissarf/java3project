package javaproject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import javaproject.restaurantfood.Beef;
import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Tea;

public class OrderTest {
    
    @Test
    public void testGetters() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        ShoppingCart sc = new ShoppingCart();
        sc.addItem(drink, 2);
        Customer customer = new Customer("test","alex","123","montreal"); 
        Order order = new Order(sc, customer);
        boolean works=false;
        if (order.getTotalPrice()==7.00){
            works=true;
        }
        assertTrue(works);
    }
}