package javaproject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Tea;

public class ShopCartItemTest {
    @Test
    public void testConstructor() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        ShopCartItem sci = new ShopCartItem(drink, 1);
        boolean works = false;
        if(sci.getFood().equals(drink)) {
            works = true;
        }
        assertTrue(works);
    }
}
