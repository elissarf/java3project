package javaproject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Tea;

public class ShoppingCartTest{
    @Test
public void testAdd() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        ShoppingCart sc = new ShoppingCart();
        boolean works = false;
        sc.addItem(drink,1);
        if(sc.getShopCartItems().get(0).getFood().equals(drink)) {
            works = true;
        }
        assertTrue(works);
    }
    @Test
    public void testremove() {
        RestaurantFood drink = new Tea("green tea", 3.50, 20, "water,tea");
        ShoppingCart sc = new ShoppingCart();
        RestaurantFood drink2 = new Tea("iced tea", 3.50, 20, "water,tea");
        sc.addItem(drink, 1);
        sc.removeItem(drink);
        sc.addItem(drink2, 1);
        boolean works = false;
        if(sc.getShopCartItems().get(0).getFood().equals(drink2)) {
            works = true;
        }
        assertTrue(works);
    }

}
