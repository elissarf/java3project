package javaproject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import javaproject.restaurantfood.RestaurantFood;
import javaproject.restaurantfood.Rice;

public class SideTest {
    public static final double DELTA = 1e-15;
    /**
     * Test calculateCalories() to check if it returns the total calories of the example side
     */
    @Test
    public void testCalculateCalories() {
        RestaurantFood side = new Rice("white rice", 5.00, 50, "rice");
        assertEquals(50, side.calculateCalories());
    }
    /**
     * Test calculatePrice() to check if it returns the total price of the example side
     */
    @Test
    public void testCalculatePrice() {
        RestaurantFood side = new Rice("white rice", 5.00, 50, "rice");
        assertEquals(5.00, side.calculatePrice(),DELTA);
    }

    /**
     * Test describe() to check if it returns an string which has all of the ingredients of the example side 
     */
    @Test
    public void testDescribe() {
        RestaurantFood side = new Rice("white rice", 5.00, 50, "just rice");
        String expected = "just rice";
        assertEquals( expected,side.describe());
    }
}
